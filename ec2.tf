provider "aws" {
    region = "ap-south-1"
    profile = "default"
}

resource "aws_instance" "myec2"{
    ami = "ami-02e60be79e78fef21"
    instance_type = "t2.micro"
    subnet_id = "subnet-f4b6b69c"
    key_name = "rajesh"
    security_groups = ["sg-0ab2f710e90fc404c"]
    tags = {
        Name = "jenkins-terra"
    }   

}